#include "web-map-xpress.h"

int main(int argv, char **args) {
    QApplication app(argv, args);

    WebMapXpress wmx;
    wmx.show();

    return app.exec();
}
