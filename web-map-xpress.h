#include <QtGui>
#include <QtWebKit/QWebView>

class WebMapXpress: public QMainWindow
{
    Q_OBJECT

public:
    WebMapXpress();

private slots:
    void save();
    void open();

private:
    QWebView *webView;
    QAction *openAction;
    QAction *saveAction;
    QAction *exitAction;

    QMenu *fileMenu;

};
