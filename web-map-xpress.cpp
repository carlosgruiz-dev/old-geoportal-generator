#include "web-map-xpress.h"

WebMapXpress::WebMapXpress() {
    openAction = new QAction(tr("Cargar &Datos"),this);
    saveAction = new QAction(tr("&Guardar"),this);
    exitAction = new QAction(tr("&Salir"),this);

    connect(openAction, SIGNAL(triggered()), this, SLOT(open()));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(save()));
    connect(exitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

    fileMenu = menuBar()->addMenu(tr("&Archivo"));
    fileMenu->addAction(openAction);
    fileMenu->addAction(saveAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    webView = new QWebView();
    webView->setHtml(QString("<!DOCTYPE html><html><head><title>geoportal</title><style> \n\
body { margin: 0px; } .cont { margin: 0 auto;border: 1px solid #ccc;width: 55em; } \n\
.header { width: 100%;height: 10em;background: #CCC; } .explain { font-family: Arial,Verdana,Helvica;font-size: 12px;margin: 5px;text-align: justify; } \n\
.toolbar { margin: 5px; } .map { width: 100%;height: 45em;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc; } \n\
.footer { font-family: Arial,Verdana,Helvica;font-size: 10px;text-align: center; } </style> \n\
<link rel='stylesheet' href='http://openlayers.org/dev/theme/default/style.css' type='text/css'> \n\
<script src='http://openlayers.org/dev/OpenLayers.js'></script> <script language='javascript'> \n\
function demo() { alert('funcionalidad pendiente');} var map, layer; \n\
function init(){ map = new OpenLayers.Map('map'); layer = new OpenLayers.Layer.OSM('Simple OSM Map'); \n\
map.addLayer(layer); map.setCenter( new OpenLayers.LonLat(-65.72803, 7.92773).transform(\n\
new OpenLayers.Projection('EPSG:4326'),map.getProjectionObject()), 6); } </script></head>\n\
<body onload='init()'><div class='cont'><div class='header'></div><div class='explain'><h1>geoportal</h1>\n\
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non ultricies velit. Maecenas ultrices diam vel enim pellentesque sed lacinia ante vestibulum. Nulla eu tortor ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam non ultrices neque. Pellentesque vitae dapibus ante. Donec dui elit, accumsan sed ullamcorper posuere, suscipit consectetur nibh. In id turpis nibh. Etiam dictum tortor vitae dolor mollis quis eleifend augue pharetra. Vestibulum non lorem ante, id varius mauris. Donec nec ligula non dolor bibendum elementum sed at leo. Donec bibendum tincidunt arcu, adipiscing dapibus ipsum volutpat ac. Duis iaculis, nunc at consequat iaculis, elit sapien tincidunt erat, pretium cursus nisi ante a tellus. Maecenas nunc sem, posuere et aliquet ut, vulputate nec tortor. Pellentesque nec massa ut velit convallis congue.</p> \n\
<p>Suspendisse adipiscing sapien sit amet eros consequat auctor. Sed vulputate massa sit amet purus elementum consequat. Mauris ac molestie odio. Nam urna ligula, eleifend eget hendrerit in, laoreet eu diam. Proin felis enim, consequat non porttitor eu, aliquet vel risus. Nunc pharetra aliquam mattis. Ut ac dui eu urna mollis porttitor id vitae dolor. Aliquam erat volutpat. Nulla tempor iaculis auctor. Donec non odio eu diam venenatis euismod. Cras pretium ante ut nunc sodales iaculis. Pellentesque tincidunt, metus sit amet auctor tincidunt, erat nunc ullamcorper nisl, id ornare velit felis id tellus. Fusce vel hendrerit lacus.</p>\n\
</div><div class='toolbar'><button onclick='demo()'>acercar</button> \n\
<button onclick='demo()'>alejar</button><button onclick='demo()'>informaci&oacute;n</button> \n\
<input type='text' placeholder=' Buscar' /><button  onclick='demo()'>ok</button></div><div id='map' class='map'></div> \n\
<div class='footer'>&copy;2012, <a href='http://arah.at'>arah.at</a></div></div></body></html>"));
    setCentralWidget(webView);

    setWindowTitle(tr("WebMap Express"));
}

void WebMapXpress::open() {
    QMessageBox::warning(this,"Información","Este es un concepto del programa final,\n actualmente simula la carga de datos");
    webView->setHtml(QString("<!DOCTYPE html><html><head><title>geoportal</title><style> \n\
body { margin: 0px; } .cont { margin: 0 auto;border: 1px solid #ccc;width: 55em; } \n\
.header { width: 100%;height: 10em;background: #CCC; } .explain { font-family: Arial,Verdana,Helvica;font-size: 12px;margin: 5px;text-align: justify; }\n\
.toolbar { margin: 5px; } .map { width: 100%;height: 45em;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc; }\n\
.footer { font-family: Arial,Verdana,Helvica;font-size: 10px;text-align: center; } </style>\n\
<link rel='stylesheet' href='http://openlayers.org/dev/theme/default/style.css' type='text/css'>\n\
<script src='http://openlayers.org/dev/OpenLayers.js'></script><script language='javascript'>\n\
function demo() { alert('funcionalidad pendiente');} var map, layer; \n\
function init(){ map = new OpenLayers.Map('map'); layer = new OpenLayers.Layer.OSM('Simple OSM Map'); \n\
map.addLayer(layer); var markers = new OpenLayers.Layer.Markers( 'Markers' ); map.addLayer(markers);\n\
var size = new OpenLayers.Size(21,25); var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);\n\
var icon1 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png',size,offset);\n\
var icon2 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker-green.png',size,offset); \n\
var icon3 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker-gold.png',size,offset);\n\
markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(-66.91180,10.48299).transform(\n\
new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject()),icon1));\n\
markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(-63.55273,8.11692).transform( \n\
new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject()),icon2));\n\
markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(-71.13879,8.59247).transform( \n\
new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject()),icon3));\n\
map.setCenter(  new OpenLayers.LonLat(-65.72803, 7.92773).transform( new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject()), 6); }</script> \n\
</head><body onload='init()'><div class='cont'><div class='header'></div><div class='explain'><h1>geoportal</h1>\n\
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non ultricies velit. Maecenas ultrices diam vel enim pellentesque sed lacinia ante vestibulum. Nulla eu tortor ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam non ultrices neque. Pellentesque vitae dapibus ante. Donec dui elit, accumsan sed ullamcorper posuere, suscipit consectetur nibh. In id turpis nibh. Etiam dictum tortor vitae dolor mollis quis eleifend augue pharetra. Vestibulum non lorem ante, id varius mauris. Donec nec ligula non dolor bibendum elementum sed at leo. Donec bibendum tincidunt arcu, adipiscing dapibus ipsum volutpat ac. Duis iaculis, nunc at consequat iaculis, elit sapien tincidunt erat, pretium cursus nisi ante a tellus. Maecenas nunc sem, posuere et aliquet ut, vulputate nec tortor. Pellentesque nec massa ut velit convallis congue.</p> \n\
<p>Suspendisse adipiscing sapien sit amet eros consequat auctor. Sed vulputate massa sit amet purus elementum consequat. Mauris ac molestie odio. Nam urna ligula, eleifend eget hendrerit in, laoreet eu diam. Proin felis enim, consequat non porttitor eu, aliquet vel risus. Nunc pharetra aliquam mattis. Ut ac dui eu urna mollis porttitor id vitae dolor. Aliquam erat volutpat. Nulla tempor iaculis auctor. Donec non odio eu diam venenatis euismod. Cras pretium ante ut nunc sodales iaculis. Pellentesque tincidunt, metus sit amet auctor tincidunt, erat nunc ullamcorper nisl, id ornare velit felis id tellus. Fusce vel hendrerit lacus. </p> \n\
</div><div class='toolbar'><button onclick='demo()'>acercar</button>\n\
<button onclick='demo()'>alejar</button><button onclick='demo()'>informaci&oacute;n</button> \n\
<input type='text' placeholder=' Buscar' /><button  onclick='demo()'>ok</button></div> \n\
<div id='map' class='map'></div><div class='footer'>&copy;2012, <a href='http://arah.at'>arah.at</a></div></div></body><html>"));

}

void WebMapXpress::save() {
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Files"), "",
        tr("HTML Files (*.html)"));

    if (fileName != "") {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::critical(this,tr("Error"), tr("Could not save this file"));
            return;
        } else {
            QString html = QString("<!DOCTYPE html> \n\
<html> \n\
<head>\n\
  <title>geoportal</title>\n\
    <style>\n\
      body {\n\
        margin: 0px;\n\
      }\n\
      .cont {\n\
        margin: 0 auto;\n\
        border: 1px solid #ccc;\n\
        width: 55em;\n\
      }\n\
      .header {\n\
        width: 100%;\n\
        height: 10em;\n\
        background: #CCC;\n\
      }\n\
      .explain {\n\
        font-family: Arial,Verdana,Helvica;\n\
        font-size: 12px;\n\
        margin: 5px;\n\
        text-align: justify;\n\
      }\n\
      .toolbar {\n\
        margin: 5px;\n\
      }\n\
      .map { \n\
        width: 100%; \n\
        height: 45em; \n\
        border-top: 1px solid #ccc;\n\
        border-bottom: 1px solid #ccc; \n\
      }\n\
      .footer {\n\
        font-family: Arial,Verdana,Helvica;\n\
        font-size: 10px;\n\
        text-align: center;\n\
      }\n\
      .band {\n\
        position: absolute;\n\
        top: 30px;\n\
        left: -150px;\n\
        z-index: 2;\n\
        background: red;\n\
        color: white;\n\
        font-family: Arial,Verdana,Helvica;\n\
        font-size: 24px;\n\
        text-align: center;\n\
        margin: 10px;\n\
        width: 400px;\n\
        transform:rotate(-45deg);\n\
        -ms-transform:rotate(-45deg); /* Internet Explorer */\n\
        -moz-transform:rotate(-45deg); /* Firefox */\n\
        -webkit-transform:rotate(-45deg); /* Safari and Chrome */\n\
        -o-transform:rotate(-55deg); /* Opera */\n\
      }\n\
    </style>\n\
    <link rel='stylesheet' href='http://openlayers.org/dev/theme/default/style.css' type='text/css'>\n\
    <script src='http://openlayers.org/dev/OpenLayers.js'></script>\n\
    <script language='javascript'>\n\
    function demo() { alert('funcionalidad pendiente');}\n\
    var map, layer; \n\
    function init(){ \n\
      map = new OpenLayers.Map('map');\n\
      layer = new OpenLayers.Layer.OSM('Simple OSM Map'); \n\
      map.addLayer(layer); \n\
      var markers = new OpenLayers.Layer.Markers( 'Markers' );\n\
      map.addLayer(markers);\n\
      var size = new OpenLayers.Size(21,25);\n\
      var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);\n\
      var icon1 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png',size,offset);\n\
      var icon2 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker-green.png',size,offset);\n\
      var icon3 = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker-gold.png',size,offset);\n\
      markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(-66.91180,10.48299).transform( \n\
        new OpenLayers.Projection('EPSG:4326'), \n\
        map.getProjectionObject()),icon1));\n\
      markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(-63.55273,8.11692).transform( \n\
        new OpenLayers.Projection('EPSG:4326'), \n\
        map.getProjectionObject()),icon2));\n\
      markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(-71.13879,8.59247).transform( \n\
        new OpenLayers.Projection('EPSG:4326'), \n\
        map.getProjectionObject()),icon3));\n\
      map.setCenter( \n\
        new OpenLayers.LonLat(-65.72803, 7.92773).transform( \n\
          new OpenLayers.Projection('EPSG:4326'), \n\
          map.getProjectionObject()), 6); \n\
    }\n\
    </script> \n\
  </head>\n\
  <body onload='init()'>\n\
    <div class='cont'>    \n\
      <div class='header'></div> \n\
      <div class='explain'>\n\
        <h1>geoportal</h1>\n\
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum non ultricies velit. Maecenas ultrices diam vel enim pellentesque sed lacinia ante vestibulum. Nulla eu tortor ante. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam non ultrices neque. Pellentesque vitae dapibus ante. Donec dui elit, accumsan sed ullamcorper posuere, suscipit consectetur nibh. In id turpis nibh. Etiam dictum tortor vitae dolor mollis quis eleifend augue pharetra. Vestibulum non lorem ante, id varius mauris. Donec nec ligula non dolor bibendum elementum sed at leo. Donec bibendum tincidunt arcu, adipiscing dapibus ipsum volutpat ac. Duis iaculis, nunc at consequat iaculis, elit sapien tincidunt erat, pretium cursus nisi ante a tellus. Maecenas nunc sem, posuere et aliquet ut, vulputate nec tortor. Pellentesque nec massa ut velit convallis congue.</p>\n\
        <p>Suspendisse adipiscing sapien sit amet eros consequat auctor. Sed vulputate massa sit amet purus elementum consequat. Mauris ac molestie odio. Nam urna ligula, eleifend eget hendrerit in, laoreet eu diam. Proin felis enim, consequat non porttitor eu, aliquet vel risus. Nunc pharetra aliquam mattis. Ut ac dui eu urna mollis porttitor id vitae dolor. Aliquam erat volutpat. Nulla tempor iaculis auctor. Donec non odio eu diam venenatis euismod. Cras pretium ante ut nunc sodales iaculis. Pellentesque tincidunt, metus sit amet auctor tincidunt, erat nunc ullamcorper nisl, id ornare velit felis id tellus. Fusce vel hendrerit lacus.</p> \n\
      </div>\n\
      <div class='toolbar'>\n\
        <button onclick='demo()'>acercar</button>\n\
        <button onclick='demo()'>alejar</button>\n\
        <button onclick='demo()'>informaci&oacute;n</button>\n\
        <input type='text' placeholder=' Buscar' />\n\
        <button  onclick='demo()'>ok</button>\n\
      </div>\n\
      <div id='map' class='map'></div>\n\
      <div class='footer'> \n\
        &copy;2012, <a href='http://arah.at'>arah.at</a>\n\
      </div>\n\
    </div>\n\
    <div class='band'> Concepto!</div>\n\
</body>\n\
</html>");
            QTextStream stream(&file);
            stream << html;
            stream.flush();
            file.close();
        }
    }
}

